.. fastash documentation master file, created by
   sphinx-quickstart on Mon Dec 20 18:17:01 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FastASH
=======

This Python 3.5+ package implements Averaged Shifted Histograms.
This density estimation method is implemented through cython.

Installation
------------

FastASH is available through `PyPI <https://pypi.org/project/fastash/>`_, and may be installed using ``pip``: ::

   $ pip install fastash

Note that if Cython is installed, the sources files in '.pyx' are used. Otherwise, the '.cpp' files are directly used.


Example code
------------

Here's an example showing the usage of :class:`~fastash.ASH`.

.. plot::
   :include-source:

   from fastash import ASH
   from scipy.stats import norm
   import numpy as np

   # Generate a distribution and draw 3**6 data points
   np.random.seed(42)
   dist = norm(loc=0, scale=1)
   X = dist.rvs(3**6)
   
   # fit the model
   ash = ASH().fit(X)
   
   # generate an evaluation
   x_eval = np.linspace(-4,4,300)
   
   plt.plot(x_eval, dist.pdf(x_eval), label='True distribution')
   plt.plot(x_eval, ash.predict(x_eval), label='ASH estimate')
   plt.legend()

Table of contents
-----------------

.. toctree::
   :maxdepth: 2
   
   introduction
   API

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
