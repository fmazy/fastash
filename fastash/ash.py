import numpy as np

import fastash.ashfunc
import hyperclip.hyperfunc
from .whitening_transformer import WhiteningTransformer
from hyperclip import Hyperplane, Hyperclip
from joblib import Parallel, delayed, dump, load
import pandas as pd
import numpy_groupies as npg
# import sparse
import time
import os
import shutil
from matplotlib import pyplot as plt

def count_diff(A):
    n, d = A.shape
    
    B = np.ones_like(A)
    
    for i in range(n - 1)[::-1]:
        for j in range(d):
            if A[i,j] == A[i+1, j]:
                if j == 0:
                    B[i,j] = B[i+1,j] + 1
                elif B[i, j-1] > 1:
                    B[i,j] = B[i+1,j] + 1
    return(B)

def discretize(X, x_min, dx):
    Z = ((X - x_min) / dx).astype(int)
    return(Z)

def compute_centers(Z, x_min, dx):
    C = x_min + Z * dx + 0.5 * dx
    return(C)

class BKDE():
    def __init__(self, 
                 h='scott', 
                 q=11, 
                 bounds=[],
                 verbose=0):
        if q%2 == 0:
            raise(ValueError("Unexpected q value. q should be an odd int."))
            
        self.h = h
        self._h = None
        self.q = q
        self.bounds = bounds
        self.verbose = verbose
    
    def fit(self, X):
        if self.verbose > 0:
            print('BKDE fit process, X.shape=', X.shape)
        # preprocessing
        if len(X.shape) == 1:
            X = X[:,None]
        
        self._real_X_min = np.min(X, axis=0)
        self._real_X_max = np.max(X, axis=0)
        
        # get data dimensions
        self._n, self._d = X.shape
        
        # preprocessing
        self._wt = WhiteningTransformer()
        X = self._wt.fit_transform(X)
        
        self._x_min = np.min(X, axis=0)
        
        # BOUNDARIES INFORMATIONS
        A, R = self._set_boundaries(x = X[0])
        self.A = A
        self.R = R
        # BANDWIDTH SELECTION
        self._compute_bandwidth(X)
        
        self._x_min = X.min(axis=0)
        dx = self._h / self.q
        
        Z = discretize(X, self._x_min, dx=dx).astype(np.intc)
        Z = pd.DataFrame(Z)
        
        U_nu = Z.groupby(by=Z.columns.tolist()).size().reset_index(name="nu")
        
        self._U = U_nu[Z.columns.tolist()].values.astype(np.intc)
        self._nu = U_nu["nu"].values.astype(np.double)
        
        print('U.shape=', self._U.shape)
        
        self._U_diff_asc = np.ones((self._U.shape[0], self._d), dtype=np.intc)
        fastash.ashfunc.count_diff_asc(self._U, self._U_diff_asc)
        
        self._U_diff_desc = np.ones((self._U.shape[0], self._d), dtype=np.intc)
        fastash.ashfunc.count_diff_desc(self._U, self._U_diff_desc)
        
        C = compute_centers(self._U, self._x_min, dx).astype(np.double)
        self._nu /= np.array(hyperclip.hyperfunc.volumes(A, R, C, self._h))
        
        return(self)
        
    def predict(self, X):
        st_all = time.time()
        if self.verbose > 0:
            print('BKDE predict process, X.shape=', X.shape)
        X = self._wt.transform(X)
        
        id_out_of_bounds = np.zeros(X.shape[0]).astype(np.bool)
        for hyp in self._bounds_hyperplanes:
            id_out_of_bounds = np.any((id_out_of_bounds, ~hyp.side(X)), axis=0)
        st = time.time()
        Z = discretize(X, self._x_min, dx=self._h / self.q)
        print('discretize time', time.time()-st)
        # sort Z
        Z = pd.DataFrame(Z)
        Z['j'] = np.arange(Z.shape[0])
        print('sort')
        st = time.time()
        Z = Z.sort_values(by=[i for i in range(self._d)])
        print('sort done in', time.time()-st)
        Z_indices = Z['j'].values.astype(np.intc)
        Z = Z[[i for i in range(self._d)]].values.astype(np.intc)
        
        print('U.shape', self._U.shape)
        print('Z.shape=', Z.shape)
        Z_diff_asc = np.ones((Z.shape[0], self._d), dtype=np.intc)
        fastash.ashfunc.count_diff_asc(Z, Z_diff_asc)
        
        Z_diff_desc = np.ones((Z.shape[0], self._d), dtype=np.intc)
        fastash.ashfunc.count_diff_desc(Z, Z_diff_desc)
        time.sleep(0.5)
        st = time.time()
        f = np.array(fastash.ashfunc.merge(U=self._U,
                                           U_diff_asc = self._U_diff_asc,
                                           U_diff_desc = self._U_diff_desc,
                                           nu=self._nu,
                                           Z=Z,
                                           Z_indices=Z_indices,
                                           Z_diff_asc=Z_diff_asc,
                                           Z_diff_desc=Z_diff_desc,
                                           q=self.q,
                                           h=self._h))
        print('merge in ', time.time()-st)
        f[id_out_of_bounds] = 0.0
        
        f = f / (self._h ** self._d * self._n)
        
        f /= self._wt.scale_
        print(time.time()-st_all)
        return(f)
    
    def _set_boundaries(self, x):
        self._bounds_hyperplanes = []

        for k, pos in self.bounds:
            if pos == 'left':
                self._add_boundary(k=k,
                                   value=self._real_X_min[k],
                                   x=x)
            elif pos == 'right':
                self._add_boundary(k=k,
                                   value=self._real_X_max[k],
                                   x=x)
            elif pos == 'both':
                self._add_boundary(k=k,
                                   value=self._real_X_min[k],
                                   x=x)
                self._add_boundary(k=k,
                                   value=self._real_X_max[k],
                                   x=x)
            else:
                raise(TypeError('Unexpected bounds parameters'))
        
        A = np.zeros((self._d, len(self._bounds_hyperplanes)))
        R = np.zeros(len(self._bounds_hyperplanes))
        
        for i_hyp, hyp in enumerate(self._bounds_hyperplanes):
            A[:, i_hyp] = hyp.a
            R[i_hyp] = hyp.r
        
        return(A, R)
    
    def _add_boundary(self, k, value, x):
        P = np.diag(np.ones(self._d))
        
        P[:, k] = value

        P_wt = self._wt.transform(P)

        hyp = Hyperplane().set_by_points(P_wt)
        hyp.set_positive_side(x)
        self._bounds_hyperplanes.append(hyp)
    
    def _compute_bandwidth(self, X):
        if type(self.h) is int or type(self.h) is float:
            self._h = float(self.h)

        elif type(self.h) is str:
            if self.h == 'scott' or self.h == 'silverman':
                # the scott rule is based on gaussian kernel
                # the support of the gaussian kernel to have 99%
                # of the density is 2.576
                self._h = 2.576 * scotts_rule(X)
            else:
                raise (ValueError("Unexpected bandwidth selection method."))
        else:
            raise (TypeError("Unexpected bandwidth type."))

        if self.verbose > 0:
            print('Bandwidth selection done : h=' + str(self._h))
            
    def set_params(self, **params):
        """
        Set parameters.

        Parameters
        ----------
        **params : kwargs
            Parameters et values to set.

        Returns
        -------
        self : DensityEstimator
            The self object.

        """
        for param, value in params.items():
            setattr(self, param, value)
    
    

class ASH():
    """
    This class implements the averaged shifted histogram method.
    
    Parameters
    ----------
    h : {'scott'} or float, default='scott'
        The bin width value. If ``'scott'``, the Scott's rule is applied.
    
    q : int, default=100
        The number of histograms
    
    bounds : list of (feature_id, type)
        The bounds of the case. For exemple, if it is a 2d case with the second feature low bounded, the parameters should be ``[1, 'left']``. The type can be 'left', 'right' or 'both'.
    
    n_mc : int, default==10**4
        The number of elements of the MonteCarlo process used in the boundaries correction algorithm.
        
    n_jobs : int, default=1
        The number of parallel jobs to run for neighbors search.
        ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context.
        ``-1`` means using all processors.
    
    verbose : int, default=0
        Verbosity level.
    """
    def __init__(self,
                 h='scott',
                 q=100,
                 bounds = [],
                 n_mc = 10**4,
                 n_jobs = 1,
                 verbose=0):

        self.h = h
        self._h = None
        self.q = q
        self.n_mc = n_mc
        self.bounds = bounds 
        self.n_jobs = n_jobs
        self.verbose = verbose

    def __repr__(self):
        if self._h is None:
            return('ASH(h='+str(self.h)+')')
        else:
            return('ASH(h='+str(self._h)+')')

    def fit(self, X):
        """
        fit
        """
        # preprocessing
        if len(X.shape) == 1:
            X = X[:,None]
        
        self._real_X_min = np.min(X, axis=0)
        self._real_X_max = np.max(X, axis=0)
        
        # get data dimensions
        self._n, self._d = X.shape
        
        # preprocessing
        self._wt = WhiteningTransformer()
        X = self._wt.fit_transform(X)
        
        self._x_min = np.min(X, axis=0)
        
        # BOUNDARIES INFORMATIONS
        A, R = self._set_boundaries(x = X[0])

        # BANDWIDTH SELECTION
        self._compute_bandwidth(X)
        
        self._x_min = X.min(axis=0)
        
        Z = discretize(X, self._x_min, h=self._h / self.q, shift=0)
        
        Z = pd.DataFrame(Z)
        U_nu = Z.groupby(by=Z.columns.tolist()).size().reset_index(name="nu")
        
        self._U = U_nu[Z.columns.tolist()].values
        self._nu = U_nu["nu"].values
        
        self._U_diff = count_diff(self._U)
        
        # # <-- job lib        
        # folder = './.temp_joblib_memmap'
        # try:
        #     os.mkdir(folder)
        # except FileExistsError:
        #     pass
        
        # X_filename_memmap = os.path.join(folder, 'X_memmap')
        # dump(X, X_filename_memmap)
        # X_memmap = load(X_filename_memmap, mmap_mode='r')
        
        # dh = self._h / self.q
        # self._U_N = Parallel(n_jobs=self.n_jobs)(delayed(get_U_N)(X_memmap, 
        #                                                           self._h, 
        #                                                           i_shift * dh) for i_shift in range(self.q))
        
        # try:
        #     shutil.rmtree(folder)
        # except:  # noqa
        #     print('Could not clean-up automatically.')
        
        # # end job lib -->
        
        return(self)
    
    def predict(self, X):
        X = self._wt.transform(X)
        
        Z = discretize(X, self._x_min, h=self._h / self.q, shift=0)
        
        id_out_of_bounds = np.zeros(X.shape[0]).astype(np.bool)
        for hyp in self._bounds_hyperplanes:
            id_out_of_bounds = np.any((id_out_of_bounds, ~hyp.side(X)), axis=0)
        
        f = np.zeros(Z.shape[0])
        
        Z = pd.DataFrame(Z)
        Z['j'] = np.arange(Z.shape[0])
        Z = Z.sort_values(by=[i for i in range(self._d)])
        Z_indices = Z['j'].values
        Z = Z[[i for i in range(self._d)]].values
        
        # for i_U in range(self._U.shape[0]):
        #     for i_Z in range(Z.shape[0]):
        #         if np.all()
        
        # for i in range(0, Z.shape[0])[::-1]:
            
        # sparse = Sparse()
        # for i_U in range(U.shape[0]):
            # sparse.set_value(tuple(U[i_U]), nu[i_U])
        # print(Z)
        
        # a = np.meshgrid(*(np.arange(self.q * 2) for i in range(self._d)))
        # a = np.vstack([aa.flat for aa in a]).T - int(self.q / 2)
        
        # f = fastash.ashfunc.predict(Z.shape[0],
        #                             U.shape[0],
        #                             a.shape[0],
        #                             Z.shape[1],
        #                             Z.astype(np.intc), 
        #                             U.astype(np.intc), 
        #                             nu.astype(np.intc), 
        #                             a.astype(np.intc), 
        #                             self.q)
        
        # for i_Z in range(Z.shape[0]):
            # for ai in a:
                # u = Z[i_Z] + ai
                # value = sparse.get_value(tuple(u))
                
                # if value > 0:
                    # f[i_Z] += value * np.product(self.q - np.abs(Z[i_Z] - u))
        
        # i_U = 0
        # i_U_start = 0
        # for i_Z in range(Z.shape[0]):
        #     i_U = i_U_start 
        #     trigger_search_U = True
        #     trigger_first_set = True
        #     while trigger_search_U and i_U < self._U.shape[0]:
        #         trigger_set_U = True
        #         # trigger_1
        #         for j in range(self._d):
        #             if Z[i_Z, j] >= self._U[i_U, j] + self.q:
        #                 print(j, i_U, Z[i_Z], self._U[i_U], self._U_diff[i_U])
        #                 trigger_set_U = False
        #                 i_U += self._U_diff[i_U, j]
        #                 break
        #             elif Z[i_Z, j] < self._U[i_U, j] - self.q:
        #                 print('2')
        #                 trigger_set_U = False
        #                 trigger_search_U = False
        #                 i_U += self._U_diff[i_U, j]
        #                 break
                    
        #         if trigger_set_U:
        #             # print('set')
        #             f[Z_indices[i_Z]] += self._nu[i_U] * np.product(self.q - np.abs(Z[i_Z] - self._U[i_U]))
        #             if trigger_first_set:
        #                 i_U_start = i_U
        #                 trigger_first_set = False
        #             else:
        #                 i_U += 1
                    
                
        
        # i_Z = 0
        # i_Z_start = 0
        # for i_U in range(n_U):
        #     trigger_first_set = True
        #     for i_Z in range(i_Z_start, Z.shape[0]):
        #         if np.any(Z[i_Z] >= U[i_U] + self.q):
        #             pass
        #         elif np.any(Z[i_Z] < U[i_U] - self.q):
        #             pass
        #         else:
        #             f[Z_indices[i_Z]] += nu[i_U] * np.product(self.q - np.abs(Z[i_Z] - U[i_U]))
                    
        #             if trigger_first_set:
        #                 trigger_first_set = False
        #                 i_Z_start = i_Z
            
            # trigger_search_Z = True
            # trigger_first_set = True
            
            # print(Z[i_Z])
            
            # while trigger_search_Z:
            #     trigger_set_nu = True
                
            #     for j in range(self._d):
            #         if U[i_U, j] - self.q > Z[i_Z, j]:
            #             trigger_set_nu = False
            #             break
            #         elif U[i_U, j] + self.q <= Z[i_Z, j]:
            #             trigger_set_nu = False
            #             trigger_search_Z = False
            #             break
                    
            #     if trigger_set_nu:
            #         f[Z_indices[i_Z]] += nu[i_U] * np.product(np.abs(Z[i_Z] - U[i_U]) - self.q)
            #         if trigger_first_set:
            #             i_Z_start = i_Z
            #         trigger_first_set = False
            #     i_Z += 1
            #     if i_Z >=Z.shape[0]:
            #         trigger_search_Z = False
            # i_Z = i_Z_start
        
        # i_U = 0
        # i_U_start = 0
        # for i_Z in range(Z.shape[0]):
            # trigger_search_U = True
            # trigger_first_set = True
            # while trigger_search_U:
                # trigger_set_nu = True
                # for j in range(self._d):
                    # if U[i_U, j] >= Z[i_Z, j] + self.q:
                        # trigger_search_U = False
                        # trigger_set_nu = False
        #                 break
        #             elif U[i_U, j] < Z[i_Z, j] - self.q:
        #                 trigger_set_nu = False
        #                 break
                    
        #         if trigger_set_nu:
        #             f[Z_indices[i_Z]] += nu[i_U]
                
        #             if trigger_first_set:
        #                 i_U_start = i_U
        #                 trigger_first_set = False
        #         i_U += 1
        #         if i_U >= n_U:
        #             trigger_search_U = False
        #     i_U = i_U_start
        
            
        # outside bounds : equal to 0
        # f[id_out_of_bounds] = 0    
        
        # f = f / (self.q ** self._d * self._h ** self._d * self._n)
        
        # f /= self._wt.scale_
        
        return(Z, f)
    
    def predict2(self, X, y=None):
        """
        predict
        """
        if len(X.shape) == 1:
            X = X[:,None]
        
        X = self._wt.transform(X)
        
        id_out_of_bounds = np.zeros(X.shape[0]).astype(np.bool)
        for hyp in self._bounds_hyperplanes:
            id_out_of_bounds = np.any((id_out_of_bounds, ~hyp.side(X)), axis=0)
        
        f = np.zeros(X.shape[0])
        # folder = './.temp_joblib_memmap'
        # try:
            # os.mkdir(folder)
        # except FileExistsError:
            # pass
        
        # X_filename_memmap = os.path.join(folder, 'X_memmap')
        # dump(X, X_filename_memmap)
        # X_memmap = load(X_filename_memmap, mmap_mode='r')
        
        # f = Parallel(n_jobs=self.n_jobs)(delayed(fastash.ashfunc.merge_predict_shift)(
        #     self._fit_results[i_shift][0], # X_digit_uniques
        #     self._fit_results[i_shift][1], # P
        #     X_memmap, # Y
        #     self._x_min, # x_min
        #     self._h, # h
        #     self._fit_results[i_shift][2], # shift
        #     ) for i_shift in range(self.q))
        
        f = np.sum(f, axis=0)
        f = f / (self.q * self._h ** self._d)
        
        # outside bounds : equal to 0
        f[id_out_of_bounds] = 0

        # Preprocessing correction
        f /= self._wt.scale_
        
        try:
            shutil.rmtree(folder)
        except:  # noqa
            print('Could not clean-up automatically.')
        
        return(f)
    
    def _set_boundaries(self, x):
        self._bounds_hyperplanes = []

        for k, pos in self.bounds:
            if pos == 'left':
                self._add_boundary(k=k,
                                   value=self._real_X_min[k],
                                   x=x)
            elif pos == 'right':
                self._add_boundary(k=k,
                                   value=self._real_X_max[k],
                                   x=x)
            elif pos == 'both':
                self._add_boundary(k=k,
                                   value=self._real_X_min[k],
                                   x=x)
                self._add_boundary(k=k,
                                   value=self._real_X_max[k],
                                   x=x)
            else:
                raise(TypeError('Unexpected bounds parameters'))
        
        A = np.zeros((self._d, len(self._bounds_hyperplanes)))
        R = np.zeros(len(self._bounds_hyperplanes))
        
        for i_hyp, hyp in enumerate(self._bounds_hyperplanes):
            A[:, i_hyp] = hyp.a
            R[i_hyp] = hyp.r
        
        return(A, R)
    
    def _add_boundary(self, k, value, x):
        P = np.diag(np.ones(self._d))
        
        P[:, k] = value

        P_wt = self._wt.transform(P)

        hyp = Hyperplane().set_by_points(P_wt)
        hyp.set_positive_side(x)
        self._bounds_hyperplanes.append(hyp)
    
    def _compute_bandwidth(self, X):
        if type(self.h) is int or type(self.h) is float:
            self._h = float(self.h)

        elif type(self.h) is str:
            if self.h == 'scott' or self.h == 'silverman':
                # the scott rule is based on gaussian kernel
                # the support of the gaussian kernel to have 99%
                # of the density is 2.576
                self._h = 2.576 * scotts_rule(X)
            else:
                raise (ValueError("Unexpected bandwidth selection method."))
        else:
            raise (TypeError("Unexpected bandwidth type."))

        if self.verbose > 0:
            print('Bandwidth selection done : h=' + str(self._h))

    def set_params(self, **params):
        """
        Set parameters.

        Parameters
        ----------
        **params : kwargs
            Parameters et values to set.

        Returns
        -------
        self : DensityEstimator
            The self object.

        """
        for param, value in params.items():
            setattr(self, param, value)

def get_U_N(X, h, shift):
    Z = discretize(X, X.min(axis=0), h, shift)
    npga = npg.aggregate(Z.T, a=1, func='sum', fill_value=0, order='C')
    U = np.where(npga>0)
    
    U_N = pd.DataFrame(np.array(U).T)
    U_N["N"] = npga[U]
    print(U_N)
    return(U_N)

def scotts_rule(X):
    """
    Scott's rule according to "Multivariate density estimation", Scott 2015, p.164.
    The Silverman's rule is exactly the same in "Density estimation for statistics and data analysis", Silverman 1986, p.87.
    Parameters
    ----------
    X : numpy array of shape (n_samples, n_features).

    Returns
    -------
    h : float
        Scotts rule bandwidth. The returned bandwidth should be then factored
        by the data variance.

    """
    n = X.shape[0]
    d = X.shape[1]
    return(n**(-1/(d + 4)))



class Digitize():
    def __init__(self, dx, shift=0):
        self.dx = dx
        self.shift = shift

    def fit(self, X):
        self._d = X.shape[1]
        self._bins = [np.arange(V.min() - self.dx + self.shift,
                                V.max() + self.dx + self.shift,
                                self.dx) for V in X.T]

        return (self)

    def transform(self, X):
        X = X.copy()
        for k in range(self._d):
            X[:, k] = np.digitize(X[:, k], bins=self._bins[k])
        return (X.astype(int))

    def fit_transform(self, X):
        self.fit(X)

        return (self.transform(X))